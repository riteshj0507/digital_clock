# Digital_clock
## 🌟 This repo consists of a digital clock using the Tkinter module 
![](https://gitlab.com/riteshj0507/digital_clock/-/blob/main/guiclockoutput.png)
👉[Click here to view code 😉](https://gitlab.com/-/ide/project/riteshj0507/digital_clock/tree/main/-/clock.py/)
